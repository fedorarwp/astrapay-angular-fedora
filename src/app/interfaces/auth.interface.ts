export interface RequestLogin {
    noHp: string,
    password: string
}

export interface ResponseLogin {
    token: string,
    role: string
}