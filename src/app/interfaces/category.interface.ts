export interface AstraTrackCategories {
    id: number;
    judulKategori: string;
    iconKategori?: string;
  }

  export interface ResponseUploadPhoto {
    image: string
  }