export interface User {
  id: number;
  name: string;
  phone: string;
  username: string;
  website: string;
  email: string;
  company: {
    bs: string;
    catchPhrase: string;
    name: string;
  };
  address: {
    city: string;
    street: string;
    suite: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    };
  };
}

export interface AstraTrackUsers {
  id: number;
  nama: string;
  noHp: string;
  email: string;
  tanggalLahir: string;
  photo?: string;
  dompet: {
    saldo: number;
  };
  role: {
    roleTitle: string;
  };
  userKategoriList: [];
}

export interface ResponseUploadPhoto {
  image: string
}
