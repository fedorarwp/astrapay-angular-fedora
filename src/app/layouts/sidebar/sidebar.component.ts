import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  constructor(private router: Router) {}

  doLogout() {
    Swal.fire({
      title: 'Are you sure?🫣',
      text: "You are about to log out from this page😱",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0046e6',
      cancelButtonColor: '#E82991',
      confirmButtonText: 'Yes, log out!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire( {
          title: 'See you again!',
          text: 'You are logged out.',
          icon: 'success',
        }
        )
        localStorage.clear()
        this.router.navigate(["/login"])
      }
    })
  }

}
