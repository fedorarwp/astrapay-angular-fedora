import { Component, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { AstraTrackCategories } from 'src/app/interfaces/category.interface';
import { AuthService } from 'src/app/services/auth.service';
import { CategoryService } from 'src/app/services/category.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent {
  modalRef?: BsModalRef;

  categories: AstraTrackCategories[] = [];

  iconKategori!: string;
  iconKategoriFile!: File | null;
  formAddCategory: FormGroup;
  formEditCategory: FormGroup;
  refresh = new Subject<void>();

  categoryDetail: AstraTrackCategories = {
    id: 0,
    judulKategori: '',
    iconKategori: '',
  };

  constructor(
    private categoryService: CategoryService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAddCategory = this.formBuilder.group({
      judulKategori: ['', [Validators.required]],
      iconKategori: ['', [Validators.required]],
    });
    this.formEditCategory = this.formBuilder.group({
      id: [''],
      judulKategori: ['', [Validators.required]],
      iconKategori: '',
    });
  }

  loadData() {
    this.categoryService
      .getAstraTrackCategories()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log("refresh ya" + response);
        this.categories = response;
      });
  }

  get errorControl() {
    return this.formAddCategory.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showPreview(event: any) {
    if (event) {
      const file = event.target.files[0];
      this.iconKategoriFile = file;
      const reader = new FileReader();
      reader.onload = () => {
        this.iconKategori = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  doAddCategory() {
    if (this.iconKategoriFile) {
      this.categoryService
        .uploadPhoto(this.iconKategoriFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              judulKategori: this.formAddCategory.value.judulKategori,
              iconKategori: val.data,
            };
            this.iconKategoriFile = null;
            return this.categoryService
              .addCategory(payload)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          Swal.fire({
            title: 'Add new category successful!',
            text: 'You successfully added a new category🥳',
            icon: 'success',
            confirmButtonColor: '#0046e6',
            confirmButtonText: 'Done',
          });
          this.loadData();
        });
    }
  }

  doEditCategory() {
    if (this.iconKategoriFile) {
      this.categoryService
        .uploadPhoto(this.iconKategoriFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              id: this.formEditCategory.value.id,
              judulKategori: this.formEditCategory.value.judulKategori,
              iconKategori: val.data,
            };
            this.iconKategoriFile = null;
            return this.categoryService
              .editCategory(payload, this.categoryDetail.id)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          Swal.fire({
            title: 'Edit a category successful!',
            text: 'You successfully edited an existing category🥳',
            icon: 'success',
            confirmButtonColor: '#0046e6',
            confirmButtonText: 'Done',
          });
          this.loadData();
        });
    } else {
      const payload = {
        id: this.formEditCategory.value.id,
        judulKategori: this.formEditCategory.value.judulKategori,
        iconKategori:
          this.formEditCategory.value.iconKategori ??
          this.categoryDetail.iconKategori,
      };
      this.categoryService
        .editCategory(payload, this.categoryDetail.id)
        .pipe(map((val) => val))
        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }
  }

  getCategoryDetail(detail: any) {
    console.log(detail);
    this.categoryDetail = detail;

    this.formEditCategory.controls['id'].patchValue(detail.id);
    this.formEditCategory.controls['judulKategori'].patchValue(
      detail.judulKategori
    );
    //debugger
    //this.formEditCategory.controls['iconKategori'].patchValue(detail.iconKategori)
  }

  doDeleteKategori() {
    if (this.iconKategoriFile) {
      this.categoryService
        .uploadPhoto(this.iconKategoriFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              judulKategori: this.formEditCategory.value.iconKategori,
              iconKategori: val.data,
            };
            this.iconKategoriFile = null;
            return this.categoryService
              .deleteKategori(payload, this.categoryDetail.id)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          Swal.fire({
            title: 'Delete a category successful!',
            text: 'You successfully deleted a category🥳',
            icon: 'success',
            confirmButtonColor: '#0046e6',
            confirmButtonText: 'OK',
          });
    
          this.loadData();
        });
    } else {
      const payload = {
        judulKategori: this.formEditCategory.value.judulKategori,
        iconKategori: this.categoryDetail.iconKategori,
      };
      this.categoryService
        .deleteKategori(payload, this.categoryDetail.id)
        .pipe(map((val) => val))

        .subscribe((response) => {
          console.log(response);
          Swal.fire({
            title: 'Delete a category successful!',
            text: 'You successfully deleted a category🥳',
            icon: 'success',
            confirmButtonColor: '#0046e6',
            confirmButtonText: 'OK',
          });
          this.loadData();
        });
    }
  }

  getDeleteModal(data: any) {
    console.log(data);
    this.categoryDetail = data;
  }


}
