import { Component, TemplateRef } from '@angular/core';
import { AstraTrackUsers } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  //constructor() {}
  modalRef?: BsModalRef;

  user: AstraTrackUsers[] = [];

  photo!: string;
  photoFile!: File;
  formAdd: FormGroup;
  // formEdit: FormGroup;
  refresh = new Subject<void>();

  constructor(
    private userService: UsersService,
    private modalService: BsModalService,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAdd = this.formBuilder.group({
      noHp: ['', [Validators.required, Validators.minLength(11)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      nama: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      tanggalLahir: ['', [Validators.required]],
      //role: ['', [Validators.required]],
      photo: ['', [Validators.required]],
    });
  }

  loadData() {
    this.userService
      .getAstraTrackUsers()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.user = response;
      });
  }

  get errorControl() {
    return this.formAdd.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  //ngOnInit(): void{}

  //@Input() dataResponse: UserAstraTrack[] = []

  showPreview(event: any) {
    if (event) {
      const file = event.target.files[0];
      this.photoFile = file;
      const reader = new FileReader();
      reader.onload = () => {
        this.photo = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  doAddUser() {
    Swal.fire({
      title: 'Add new user successful!',
      text: 'You successfully added a new user',
      icon: 'success',
      confirmButtonColor: '#0046e6',
      confirmButtonText: 'Done',
    });
    this.userService
      .uploadPhoto(this.photoFile)
      .pipe(
        switchMap((val) => {
          const payload = {
            noHp: this.formAdd.value.noHp,
            password: this.formAdd.value.password,
            nama: this.formAdd.value.nama,
            email: this.formAdd.value.email,
            tanggalLahir: this.formAdd.value.tanggalLahir,
            photo: val.data,
          };
          return this.authService.register(payload).pipe(map((val) => val));
        })
      )
      .subscribe((response) => {
        console.log(response);
        this.loadData();
      });
  }

  userDetail: AstraTrackUsers = {
    id: 0,
    noHp: '',
    nama: '',
    email: '',
    tanggalLahir: '',
    photo: '',
    dompet: {
      saldo: 0,
    },
    role: {
      roleTitle: '',
    },
    userKategoriList: [],
  };

  getDetail(detail: AstraTrackUsers) {
    console.log(detail);
    this.userDetail = detail;
    // this.editForm.controls['name'].patchValue(data.name)
    // this.editForm.controls['id_category'].patchValue(data.id_category)
    // this.editForm.controls['desc_product'].patchValue(data.desc_product)
    // this.editForm.controls['stocks'].patchValue(data.stocks)
    // this.editForm.controls['price'].patchValue(data.price)
    // debugger
    // this.editForm.controls['img_src'].patchValue(data.img_src)
  }

  openModalDetail(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  // openModalDetail(detail: AstraTrackUsers) {
  //   console.log(detail)
  //   this.userDetail = detail
  // }
}
