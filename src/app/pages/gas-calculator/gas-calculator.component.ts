import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-gas-calculator',
  templateUrl: './gas-calculator.component.html',
  styleUrls: ['./gas-calculator.component.scss']
})

export class GasCalculatorComponent {

  constructor(private dataService: DataService) {
    this.harga = dataService.baseHarga
  }

  testClass = 'alert alert-success'

  harga = 0
  total = 0
  change = 0
  bensin = "pilih jenis bensin"
  // kodeBensin = 0
  showChange = true

  @Input() jumlahLiter = 0
  @Input() totalBayar = 0

  @Output() dataCallBack = new EventEmitter()

  cekHarga(value: any) {
    if (value == 'Pertamax') {
      this.harga = 13900
      this.bensin = "Pertamax"
    }
    else if (value == 'Pertalite') {
      this.harga = 10000
      this.bensin = "Pertalite"
    }
    else if (value == 'Solar') {
      this.harga = 6800
      this.bensin = "Solar"
    }
    else if (value == 'PertamaxPlus') {
      this.harga = 14300
      this.bensin = "Pertamax Plus"
    }

  }

  hitungTotal(liter: number, uang: number) {
    this.total = liter * this.harga
    if (this.total < uang) {
      this.showChange = true
      this.change = uang - this.total
    }
    else {
      this.showChange = false
    }
    
  }

}
