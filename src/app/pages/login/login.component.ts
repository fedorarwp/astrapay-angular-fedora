import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  noHp = ''
  password = ''


  formLogin: FormGroup

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) {
    this.formLogin = this.formBuilder.group({
      noHp: ['', [Validators.required]],
      password: ['', [Validators.required, , Validators.minLength(6)]]
    })
  }

  get errorControl(){
    return this.formLogin.controls;
  }

  doLogin() {
    const payload = {
      noHp: this.formLogin.value.noHp,
      password: this.formLogin.value.password
    }
    this.authService.login(payload).subscribe(
      response => {
        console.log(response)
        // if (response.token) {
        //   this.router.navigate(['/playground'])
        // }

        //alert("You are successfully logged in as a " + response.data.role)

        if (response.data.role === 'ROLE_ADMIN') {
          localStorage.setItem('token', response.data.token)
          this.router.navigate(['/admin'])
          Swal.fire({
            title: 'Login Success!🎉',
            text: `Welcome! You are logged in as a ${response.data.role} 👋`,
            icon: 'success',
            confirmButtonColor: '#0046e6',
            confirmButtonText: 'Got it'
          })
        }
        if (response.data.role === 'ROLE_USER') {
          Swal.fire({
            title: 'Nice Try!😜',
            text: 'You are not admin! Why are you here?🤨',
            icon: 'error',
            confirmButtonColor: '#0046e6',
            confirmButtonText: 'Got it'
          })
          //this.router.navigate(['/users'])
        }
        else {
          //nothing
        }
      },
      error => {
        console.log(error);
        Swal.fire({
          title: 'Login Failed!⛔️',
          text: 'Who are you?🤔',
          icon: 'error',
          confirmButtonColor: '#0046e6',
          confirmButtonText: 'Got it'
        })
        error = error.error.message
      }
    )
  }
}
