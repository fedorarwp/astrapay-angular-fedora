import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent {
  title = 'AstraPay-Angular-Fedora';

  name = "Fedora"
  age = 21
  status = true

  // constructor() {
  //   this.name = "Fefe"
  // }

  person = {
    title: 'Another profile',
    name: "FEFE",
    age: 22,
    status: true
  }

  constructor() {
    this.name = 'Ramadhanty'
    this.age = 20
  }

  datas = [1, 2, 3]
  //datas = new Array(10)
  personList = [ 
    {
      title: 'Another profile 1',
      name: "FEFE 1",
      age: 21,
      status: true
    },
    {
      title: 'Another profile 2',
      name: "FEFE 2",
      age: 22,
      status: true
    },
    {
      title: 'Another profile 3',
      name: "FEFE 3",
      age: 23,
      status: true
    }
  ]

  onCallBack(ev: any) {
    console.log(ev)
  }
  onCallBackAdd(ev: any) {
    ev.data.title = "Ini tambahan aja sih"
    this.personList.push(ev.data)
    console.log(this.personList)
  }

  showData = false

  nomor = 1
  

}
