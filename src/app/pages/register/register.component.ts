import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  noHp = '';
  password = '';
  nama = '';
  email = '';
  tanggalLahir = '';

  formRegister: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.formRegister = this.formBuilder.group({
      noHp: ['', [Validators.required, Validators.minLength(11)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      nama: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      tanggalLahir: ['', [Validators.required]],
      confirm_password: ['', [Validators.required]]
    });
  }

  get errorControl() {
    return this.formRegister.controls;
  }

  get confirmPassword(){
      return this.formRegister.value.password == this.formRegister.value.confirm_password
    }

  doRegister() {
    const payload = {
      noHp: this.formRegister.value.noHp,
      password: this.formRegister.value.password,
      nama: this.formRegister.value.nama,
      email: this.formRegister.value.email,
      tanggalLahir: this.formRegister.value.tanggalLahir,
      confirm_password: this.formRegister.value.confirm_password
    };
    this.authService.register(payload).subscribe(
      response => {
        alert('Registration Success!')
        //console.log(response)
        this.router.navigate(['/login'])
      },
      error => {
        console.log(error)
        alert(error.error.message)
      }
    )
  }
}
