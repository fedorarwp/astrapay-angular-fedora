import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { AstraTrackCategories } from '../interfaces/category.interface';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  private baseApi = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) {}

  getAstraTrackCategories(): Observable<AstraTrackCategories[]> {
    return this.httpClient
      .get<ResponseBase<AstraTrackCategories[]>>(
        `${this.baseApi}/astratrack/kategori`
      )
      .pipe(
        map((val) => {
          const data: AstraTrackCategories[] = [];
          for (let item of val.data) {
            data.push({
              id: item.id,
              judulKategori: item.judulKategori,
              iconKategori: `${this.baseApi}/astratrack/files/${item.iconKategori}:.+`,
            });
          }
          return data;
        }),
        catchError((err) => {
          console.log(err);
          throw err;
        })
      );
  }

  uploadPhoto(data: File): Observable<ResponseBase<string>> {
    const file = new FormData();
    file.append('file', data, data.name);
    return this.httpClient.post<ResponseBase<string>>(
      `${this.baseApi}/astratrack/upload-file`,
      file
    );
  }

  addCategory(payload: { judulKategori: string; iconKategori?: string }) {
    return this.httpClient.post(this.baseApi + '/astratrack/kategori', payload);
  }

  editCategory(
    payload: {
      id: number;
      judulKategori: string;
      iconKategori?: string;
    },
    id: number
  ) {
    return this.httpClient.put(
      this.baseApi + '/astratrack/kategori/' + id,
      payload
    );
  }

  deleteKategori(
    payload: { judulKategori: string; iconKategori?: string },
    id: number
  ) {
    return this.httpClient.delete(this.baseApi + '/astratrack/kategori/' + id);
  }
}
