import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, catchError } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import {
  AstraTrackUsers,
  ResponseUploadPhoto,
} from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private baseApi = 'http://localhost:8080';
  private baseApiPhoto = 'https://1cfe-125-164-20-251.ap.ngrok.io';

  constructor(private httpClient: HttpClient) {}

  getAstraTrackUsers(): Observable<AstraTrackUsers[]> {
    
    return this.httpClient
      .get<ResponseBase<AstraTrackUsers[]>>(
        `${this.baseApi}/astratrack/listusers`
      )
      .pipe(
        map((val) => {
          const data: AstraTrackUsers[] = [];
          for (let item of val.data) {
            data.push({
              id: item.id,
              nama: item.nama,
              email: item.email,
              noHp: item.noHp,
              tanggalLahir: item.tanggalLahir,
              role: item.role,
              photo: `${this.baseApi}/astratrack/files/${item.photo}:.+`,
              dompet: {
                saldo: item.dompet.saldo,
              },
              userKategoriList: item.userKategoriList,
            });
          }
          return data;
        }),
        catchError((err) => {
          console.log(err);
          throw err;
        })
      );
  }

  uploadPhoto(data: File): Observable<ResponseBase<string>> {
    const file = new FormData();
    file.append('file', data, data.name);
    return this.httpClient.post<ResponseBase<string>>(
      `${this.baseApi}/astratrack/upload-file`,
      file
    );
  }

  addUser(payload: any) {
    return this.httpClient.post<ResponseBase<any>>(
      `${this.baseApi}/users/register`,
      payload
    );
  }
}
